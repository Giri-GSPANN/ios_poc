//
//  XXXApp.swift
//  XXX
//
//  Created by Giri Thangellapally on 17/11/22.
//

import SwiftUI

@main
struct XXXApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
